package ru.threadPrior;

/**
 * Created by Dashka on 28.12.2017.
 */
public class ThreadPrior extends Thread {
    private boolean one;

    public void run() {
        for (int i = 1; i <= 200; i++) {
            System.out.println(getName() + " — " + i);
            if (i ==25) {
                setPriority(one ? 10 : 1);
            }
            try {
                sleep(10);
            } catch (InterruptedException ignored) {

            }
        }
    }

    ThreadPrior(boolean one) {
        this.one = one;
    }
}

class Main {
    public static void main(String[] args) throws InterruptedException {


        Thread thread2 = new ThreadPrior(false);
        Thread thread = new ThreadPrior(true);
        thread.setPriority(5);
        thread2.setPriority(10);

        thread.setName(" 1 поток "+" с приоритетом "+thread.getPriority());
        thread2.setName(" 2 поток "+" с приоритетом "+thread2.getPriority());


        thread2.start();
        thread.start();
        thread.join();
        thread2.join();




    }
}
