package ru.downloadimage;

import java.io.*;
import java.net.*;

/**
 *  Класс, в котором реализовано выполнение потока где находятся ссылки для скачивания.
 *  "Качаем картинки".
 */

    public class Main {

        private static final String OUTPUT_FILE_TXT = "outImage.txt";

        public static void main(String[] args) throws InterruptedException {

            Image thread = new Image(OUTPUT_FILE_TXT);
            thread.start();
        }
    }