package ru.downloadimage;

import java.io.*;
import java.net.URL;
import java.nio.channels.*;

/**
 * Класс, в котором реализовано скачивание картинок, по URL адресам
 * из текствого файла outImage.txt.
 *
 * Created by Dashka on 28.12.2017.
 */
public class Image extends Thread {

    private String resultFile;

    private static final String PATH_TO_IMAGE = "C:\\Users\\Dashka\\IdeaProjects\\img\\";

    Image(String resultFile) {
        this.resultFile = resultFile;
    }

    @Override
    public void run() {
        try (BufferedReader imageFile = new BufferedReader(new FileReader(resultFile))) {
            long beforeDownload = System.currentTimeMillis();
            String music;
            int count = 0;
            while ((music = imageFile.readLine()) != null) {
                System.out.println("Началась загрузка " + (count + 1));
                downloadUsingNIO(music, PATH_TO_IMAGE + String.valueOf(count + 1) + ".jpg");
                count++;
                long timeDownload = System.currentTimeMillis() - beforeDownload;
                System.out.println("Загрузка завершена. Время загрузки: " + (timeDownload / 1000) + " секунд");
                beforeDownload = System.currentTimeMillis();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, который по ссылке скачивает картинки.
     *
     * @param linkImage ссылка для скачивания
     */
    private static void downloadUsingNIO(String linkImage, String fileName) throws IOException {
        URL url = new URL(linkImage);
        try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
             FileOutputStream stream = new FileOutputStream(fileName)) {
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        }
    }
}

