package ru.resources;

/**
 * Created by Dashka on 19.11.2017.
 */
public class ThreadResources extends Thread{
    private OpenResources thread;

    public void run() {
        int i;
        while ((i = thread.increment()) < 1000) {
            try {
                System.out.println(i + " - " + getName());
                sleep(1);
            } catch (InterruptedException e) {
            }
        }
        System.out.println(getName() + " cancelled");
    }

    public ThreadResources (OpenResources thread) {
        this.thread = thread;
    }

}
