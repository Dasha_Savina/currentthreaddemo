package ru.resources;

/**
 * Created by Dashka on 19.11.2017.
 */
public class OpenResources {
    int i=0;

    public void start() throws Exception{
        i = 0;
        Thread thread1 = new ThreadResources(this);
        Thread thread2 = new ThreadResources(this);
        thread1.setName("1 поток");
        thread2.setName("2 поток");
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

    }

    public int increment() {
        return ++i;
    }
}


