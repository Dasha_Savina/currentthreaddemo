package ru.copy;

import java.io.IOException;

/**
 * Класс для организации параллельного копирования двух файлов.
 * А так же замерить примерное время выполнения кода.
 *
 * Created by Dashka on 24.11.2017.
 */
public class MainParallel {

        public static void main(String[] args) throws InterruptedException, IOException {
            Copy thread1 = new Copy("input.txt", "out.txt");
            Copy thread2 = new Copy("input.txt", "out2.txt");
            Copy thread3 = new Copy("input.txt", "out.txt");
            Copy thread4 = new Copy("input.txt", "out2.txt");
                    System.out.println("Общее время = " +  String.format ("%.3f",second(thread1, thread2)));
                    System.out.println("Время завершения потоков = " + (fist(thread3, thread4)));
                }

                private static double second(Copy thread1, Copy thread2) throws InterruptedException, IOException {

                    final long before = System.currentTimeMillis();//засекаем время
                    thread1.start();
                    thread2.start();

                    final long after1 = System.currentTimeMillis();
                    System.out.println("Время после запуска потоков = " + (after1 - before));
                    thread1.join();
                    thread2.join();

                    final long after = System.currentTimeMillis();
                    System.out.println(after - before);
                    System.out.println((after - before)/1000);
                    System.out.println((after - before)/1000.);
                    return (after - before)/1000.;
                }

                private static long fist(Copy thread3, Copy thread4) throws InterruptedException, IOException {

                    final long before = System.currentTimeMillis();
                    thread3.start();
                    thread3.join();
                    final long after1 = System.currentTimeMillis();
                    System.out.println("Время завершения 1 потока = " + (after1 - before));
                    thread4.start();
                    thread4.join();
                    final long after = System.currentTimeMillis();
                    System.out.println("Время завершения 2 потока = " + (after - before));
                    return (after - before) + (after1 - before);
                }
            }
