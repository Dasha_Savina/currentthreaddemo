package ru.copy;

import java.io.*;


/**
 * Класс для реализации копирования текстового файла с заданным именем
 * в текстовый файл по указанному пути.
 *
 * Created by Dashka on 23.11.2017.
 */
class Copy extends Thread {
    private String input;
    private String out;

    Copy(String input, String out) {
        this.input = input;
        this.out = out;
    }
    public void run() {
        String str;
        try {
            BufferedReader input = new BufferedReader(new FileReader(this.input));
            BufferedWriter out = new BufferedWriter(new FileWriter(this.out));
            while ((str = input.readLine()) != null) {
                    out.write(str + "\n");
            }
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



