package ru.chicken;

/**
 * Created by Dashka on 01.09.2017.
 */
public class Chicken implements Runnable {
    public static void main(String[] args) {
        Thread thread1 = Thread.currentThread();
        thread1.setPriority(5);
        Thread thread2 = (new Thread(new Chicken()));
        thread2.setPriority(5);
        thread2.start();
        double a = Math.random()*10;
        try {
            for (int i = 0; i < a; i++) {
                System.out.println("Первым появилось яйцо");
                Thread.sleep(2000);
            }
            System.out.println("Победила курица!");
            System.exit(0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        double a = Math.random()*10;
        try {
            for (int i = 0; i < a; i++) {
                System.out.println("Первым появилась курица");
                Thread.sleep(2000);
            }
            System.out.println("Победило яйцо!");
            System.exit(0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
