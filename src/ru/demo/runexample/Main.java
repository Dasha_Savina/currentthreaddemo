package ru.demo.runexample;

import java.util.Random;

/**
 * Пример запуска нескольких потоков
 *
 * Created by Dashka on 02.09.2017.
 */
public class Main {
    public static  void main(String[] args) throws  InterruptedException {
        RandomRunExample.example();
        //SeriesRunExample.example();
    }
}
