package ru.demo.hellothread;

/**
 * Класс Tread различают интерфейс Runnable
 * хотя его метод run() ничего не делает
 * Подкласс класса Thread может
 * обеспечить собственную реализацию метода run()
 * Created by Dashka on 01.09.2017.
 */
public class HelloThread extends Thread{
    public void run(){
     System.out.println("Hello from a thread!");
    }
    public static void main (String args[]){
        (new HelloThread()).start();
        System.out.println("Hello from main thread!");
    }

    }

