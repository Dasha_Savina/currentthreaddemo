package ru.demo.hellorunnable;

/**
 * Реализация интерфейса Runnable
 *
 * Интерфейс Runnable определяет один метод  run
 * предназначенный для размещения кода ,использоваемого в потоке
 * Runnable объект пересекается в конструктор Thread
 * и с помощью метода start () поток запускается
 * Created by Dashka on 03.09.2017.
 */
public class HelloRunnable implements Runnable {
    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Hello from a thread!");

    }

    public static void main(String args[]){
        (new Thread(new HelloRunnable())).start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Hello from main thread!");
        }
    }
