package ru.downloadmusic;

import java.io.*;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Класс для  чтения ,записи и скачивания музыки с сайта.
 * Created by Dashka on 01.12.2017.
 */



public class Main {

    private static final String IN_FILE_TXT = "inFile.txt";//создается переменная типа String
    private static final String OUT_FILE_TXT = "outFile.txt";
    private static final String PATH_TO_MUSIC = "C:\\Users\\Dashka\\IdeaProjects\\Muzik\\";

    private static final String urlPattern = "(?<=data-url=\")[^\"]*(?=\")";//создание переменной типа Стринг
    /**
     * Метод main.
     */
    public static void main(String[] args) {
        read();
        download();
    }
    /**
     * Метод для скачивания.
     */
    private static void download() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {//создание объекта и запись содержимого файла с расширение txt в этот объект.
            String music;
            int count = 1;
            try {
                while ((music = musicFile.readLine()) != null) {//пока есть что читать из файла ,будет выполняться тело цикла
                    Download down = new Download(music, PATH_TO_MUSIC + String.valueOf(count) + ".mp3");
                    down.start();
                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Метод для чтения и записи Url для скачивания.
     */
    private static void read() {
        String url;
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
            while ((url = inFile.readLine()) != null) {
                String result =getHTML(url);
                getAndWriteLinks(outFile, result);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Метод для  получения кода страницы с которой скачиваем песни.
     */
    private static String getHTML(String Url) throws IOException {
        URL url = new URL(Url);

        String result;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {//открытие потока
            result = bufferedReader.lines().collect(Collectors.joining("\n"));//Код страницы
        }
        return result;
    }
    /**
     * Метод для записи getAndWriteLinks для скачивания в текстовой документ.
     */
    private static void getAndWriteLinks(BufferedWriter outFile, String result) throws IOException {
        Pattern pattern = Pattern.compile(urlPattern);//регулярное выражение
        Matcher matcher = pattern.matcher(result);
        int i = 0;
        while (matcher.find() && i < 1) {//возвращает true ,если в строке ссылка совпадает с шаблоном
            outFile.write(matcher.group() + "\r\n");//записывает сслыки ,которые совпали с шаблоном
            i++;
        }
    }
}
