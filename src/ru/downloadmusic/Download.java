package ru.downloadmusic;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Класс для скачивания с использованием потока
 *
 * Created by Dashka on 13.12.2017.
 */

class Download extends Thread {
    private String strUrl;
    private String filepath;

    Download(String strUrl, String filepath) {
        this.strUrl = strUrl;
        this.filepath = filepath;
    }

    public void run() {
        try {
            URL url = new URL(strUrl);
            ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
            FileOutputStream stream = new FileOutputStream(filepath);
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            stream.close();
            byteChannel.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}


