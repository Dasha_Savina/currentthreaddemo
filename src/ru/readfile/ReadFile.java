package ru.readfile;

import java.io.*;
import java.util.concurrent.Semaphore;

/**
 * Created by Dashka on 16.11.2017.
 */
public class ReadFile extends Thread {
    Semaphore sem;
    private DataInputStream input;
    private static DataOutputStream output;

    public ReadFile(DataInputStream input, DataOutputStream output, Semaphore sem) {
        this.input = input;
        this.output = output;
        this.sem = sem;
    }

    public void run() {
        String str;
        try {
            while (input.available() != 0) {
                try {
                    sem.acquire(1);
                    str = input.readLine();
                    output.writeChars(str + "\n");
                    sem.release();
                    Thread.currentThread().sleep(1000);
                } catch (InterruptedException ex) {
                }
            }

        } catch (IOException ex) {


        }
    }
}

class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        try (DataInputStream input1 = new DataInputStream(new FileInputStream("input1.txt"))) {
            try (DataInputStream input2 = new DataInputStream(new FileInputStream("input2.txt"))) {
                try (DataInputStream input3 = new DataInputStream(new FileInputStream("input3.txt"))) {
                    try (DataOutputStream output = new DataOutputStream(new FileOutputStream("output.txt"))) {

                        Semaphore sem = new Semaphore(1);

                        ReadFile thread1 = new ReadFile(input1, output, sem);
                        ReadFile thread2 = new ReadFile(input2, output, sem);
                        ReadFile thread3 = new ReadFile(input3, output, sem);

                        thread1.start();
                        thread2.start();
                        thread3.start();

                        thread1.join();
                        thread2.join();
                        thread3.join();
                    }
                }
            }

        }
    }
}